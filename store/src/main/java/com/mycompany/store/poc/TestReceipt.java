/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.store.poc;

import model.Customer;
import model.Product;
import model.Receipt;
import model.User;

/**
 *
 * @author Lenovo
 */
public class TestReceipt {
    public static void main(String[] args) {
        User seller = new User("Tong","0805696397","password");
        Customer customer = new Customer("Time","0824658695");
        Receipt receipt = new Receipt(seller,customer);
        Product p1 = new Product(1,"Ice Thai Tea",30);
        Product p2 = new Product(2,"Matcha Green Tea",40);
        receipt.addReceiptDetail(p1,1);
        receipt.addReceiptDetail(p2,3);
        System.out.println(receipt);
        receipt.deleteReceiptDetail(0);
        System.out.println(receipt);
        receipt.addReceiptDetail(p1,2);
        System.out.println(receipt);
        receipt.addReceiptDetail(p1,2);
        System.out.println(receipt);

    }

    
}
